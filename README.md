# README #

This README documents the steps necessary to get the peak application up and running.

### What is this repository for? ###

* Custom automated peak caller for Cedars-Sinai bioinformatics. Depends Macs2, HOMER, SPP and MUSIC.
* Version 0.1
* Go to the [Bitbucket repo](https://bitbucket.org/dennishazelett/peak).
* Go to the [Docker image](https://registry.hub.docker.com/u/dennishazelett/peak/).

### How do I get set up? ###

* pull the Docker image from Dockerhub: `docker pull dennishazelett/peak`
* Configuration:
Required: a directory with bam, bedgraph, wig etc
Required: a manifest

| SAMPLE  | MARK  | FORMAT | BUILD | FILE                 |
|---------|-------|--------|-------|----------------------|
| K562    | K4M1  | BAM    | hg19  | myk562file1.bam      |
| K562    | K4M3  | WIG    | hg19  | myk562file2.wig      |
| K562    | K4M3  | BDG    | hg19  | ENCFF001XGT.bedgraph |

* Dependencies
Docker image dennishazelett/peak
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact