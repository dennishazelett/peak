# file callpeaks.py
import string, sys, os, subprocess


if __name__=='__main__':

    os.chdir('/home/user1/bam')
    
    manifest = open('/home/user1/bam/manifest.txt', 'r').readlines()
    manifest = [{'SAMPLE':line[0],
                 'MARK':line[1],
                 'FORMAT':line[2],
                 'EXPERIMENT':line[3],
                 'INPUT':line[4].strip()} for line in
                [line.split() for line in manifest]]

    macs2peaks = ['K4M1', 'K4M2', 'K4M3', 'K9AC', 'K27AC']
    musicpeaks = ['K4M1', 'K9AC', 'K27AC']

    bed_manifest = open('/home/bed/manifest.txt', 'w')
    
    for a_file in manifest:

        if a_file['FORMAT']=='BAM':
            if a_file['MARK'] in macs2peaks:
                filename_root = a_file['EXPERIMENT'].split('.')[0]
                callstring = 'macs2 callpeak -t %s -c %s -f BAM -g hs -n %s -B -q 0.01' % \
                             (a_file['EXPERIMENT'],
                              a_file['INPUT'],
                              filename_root,)
                subprocess.call(callstring, shell = True)
            if a_file['MARK'] in musicpeaks:
                # subprocess.call(shellscript for running music on the file+ input in a_file
                continue

        bed_string = '%s\t%s\thg19\t%s\n' % (a_file['SAMPLE'],
                                             a_file['MARK'],
                                             '%s.bed' % filename_root,)
        bed_manifest.write(bed_string)

    # once all peaks are called move them into the bed dir, which is an open port to the host
    subprocess.call('mv *.bed /home/user1/bed/')
    subprocess.call('mv *.narrowPeak /home/user1/bed/')
    bed_manifest.close()

    return 0
